# dachs_deploy

Docker images and deploy mechanisms for DACHS and POSTGRES.
To the date of the creation of this repository there is not 
as single tagged image with a specific version of DACHS. 
For deploying purposes and reproducibility of tests it is 
preferred to deploy a docker image with a specific version.

In this project both the DACHS docker image is built tracking
the DACHS versions in the svn server at the address 
http://svn.ari.uni-heidelberg.de/svn/gavo/python/. 
In particular, once a tag is done in this repository with a specific
version of DACHS a docker image gets created pulling that
exact version from the svn server.

Furthemore, if the environment has been specified correctly
it is possible to deploy a specific version on the specific 
environment [see section below]. 

## How to use the deploy mechanism through environment

To deploy in a new machine you have to define the 'environment' in gitlab
to do so you have to go in deployment environment and great a new
 environment clicking on the button on the top right. 

In case you set up your CI/CD already you probably will find your environment
already listed in the page `/environments`.

Once you have the enviroment defined you have to add some key variables for 
your CI/CD to work properly. Such variables are:

- DEPLOY_HOST        (SSH address of the machine)
- DEPLOY_USER        (User name used for deploying files)
- SSH_PRIVATE_KEY    (SSH private key to access to the machine)
- DACHS_CONFIG       (Configuration file for DACHS)
- SERVICE_DIR        (Directory in which to deploy the docker-compose file and the .env file)
- SERVICE_PORT       (Port on which the gavo server listens to)
- SERVICE_INPUT_PATH (Path on the deploy service where the inputs resource descriptions are stored)
- POSTGRES_DB    [optional]  (Name of the database to use internally)
- POSTGRES_PASSWORD [optional]  (Password to use inside the internal database)

You can pick in other environments to have a glance on how such parameters look like.

One that is done to ask the CI/CD to deploy in that machine you have to 
change `.gitlab-ci.yml` adding the following lines:

```
deploy-[yourenvironmentname]:
  extends:
   - .deploy
  environment:
    name: [yourenvironmentname]
```

obviously replacing `[yourenvironmentname]` with the name of the environment you 
have set up.